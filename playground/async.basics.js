console.log('Starting app');

setTimeout(() => {
  console.log('Inside of callback');
}, 2000);

setTimeout(() => {
  console.log('Second Timeout with 0 wait time');
}, 0);

console.log('Finishing up');
