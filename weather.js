const request = require('request');

var getWeather = (lat, lng, callback) => {
  request({
    url: `https://api.darksky.net/forecast/6d7a0c2f913e35e659cb9532fea25d1c/${lat},${lng}`,
    json: true
  }, (error, response, body) => {
    if (error) {
      callback('Unable to connect to forecast.io servers.');
    } else if (response.statusCode == 400) {
      callback(`${response.statusMessage}: ${body.error}`);
    } else if (response.statusCode == 403) {
      callback(`${response.statusMessage}: ${body}`);
    } else if (!error && response.statusCode == 200) {
      callback(undefined, {
        temperature: body.currently.temperature,
        apparentTemperature: body.currently.apparentTemperature,
      });
    } else {
      callback('Unable to fetch weather.');
    }
  });
};

module.exports.getWeather = getWeather;
